#!/bin/bash
initDir=$(pwd)
cd "${0%/*}"

if [[ ! -f Data_NY/Trips/2015/201503-citibike-tripdata.csv ]]
#les fichiers ne sont pas encore téléchargés
then
    ./GetDataNY.sh
fi

# #conversion ifsttar
if [[ -d Data_NY/Raw/IFSTTAR ]]
then
  csvConversion/location.py Data_NY/Trips Data_NY/Trips
  csvConversion/conversion.py Data_NY/Raw/IFSTTAR Data_NY/IFSTTAR Data_NY/Trips/
fi

#conversion GBFS
if [[ -d Data_NY/Raw/GBFS ]]
then
  csvConversion/conversion.py Data_NY/Raw/GBFS Data_NY/GBFS
fi

#conversion Openbus
if [[ -d Data_NY/Raw/Openbus ]]
then
  csvConversion/conversion.py Data_NY/Raw/Openbus Data_NY/Openbus
fi
