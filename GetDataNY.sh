ls#!/bin/bash
initDir=$(pwd)
cd "${0%/*}"

if [[ ! -d Data_NY ]]
then
    mkdir Data_NY
fi

GetTripMonth(){
    if [[ ! -d Data_NY/Trips ]]
    then
        mkdir Data_NY/Trips
    fi
    if [[ ! -d Data_NY/Trips/"$1" ]]
    then
        mkdir Data_NY/Trips/"$1"
    fi

    cd Data_NY/Trips/"$1"
    base_url="https://s3.amazonaws.com/tripdata/"
    if [[ $1 > 2016 ]]
    then
        wget "$base_url""$1""$2""-citibike-tripdata.csv.zip"
        wait
        unzip "$1""$2""-citibike-tripdata.csv.zip"
        wait
        rm "$1""$2""-citibike-tripdata.csv.zip"
    else
        wget "$base_url""$1""$2""-citibike-tripdata.zip"
        wait
        unzip "$1""$2""-citibike-tripdata.zip"
        wait
        rm "$1""$2""-citibike-tripdata.zip"
    fi
    cd ../../..
}

GetStockIFSTTAR(){
# On telecharge et on réduit les sotck files
#$1 correspond à l'année, $2 au mois
base_url="http://vlsstats.ifsttar.fr/rawdata/RawData/RawData_OLD/"
if [[ ! -d Data_NY/Raw ]]
then
    mkdir Data_NY/Raw
fi
if [[ ! -d Data_NY/Raw/IFSTTAR ]]
then
    mkdir Data_NY/Raw/IFSTTAR
fi

cd Data_NY/Raw/IFSTTAR
if [[ ! -f "index.html" ]]
then
    wget "$base_url"
fi
grep  data_all_NewYork.jjson_ index.html | while read FI; do
      file=$(echo "$FI" | grep -o "data_all_NewYork.jjson_[0-9-]*.gz" )
      file=$(echo $file | cut -d " " -f 1)
      wget "$base_url""$file"
done

rm index.html
cd $initDir
}

GetOpenBus(){
if [[ ! -d Data_NY/Raw ]]
then
    ls
    mkdir Data_NY/Raw
fi
if [[ ! -d Data_NY/Raw/Openbus ]]
then
    mkdir Data_NY/Raw/Openbus
fi
echo "Make sure to have saved OpenBus archives to Data_NY/Raw/Openbus"
}

GetGBFS(){
if [[ ! -d Data_NY/Raw ]]
then
    mkdir Data_NY/Raw
fi
if [[ ! -d Data_NY/Raw/GBFS ]]
then
    mkdir Data_NY/Raw/GBFS
fi
echo "Make sure to have saved OpenBus archives to Data_NY/Raw/GBFS"
}




#Pour 2015 on garde que les mois 03-12 pcq les stocks ne sont pas dispo pour les autres mois
#Il faudra changer ca.
year=2015
for month in $(seq 3 12)
do
    mois=$(printf %02d "$month")
    GetTripMonth "$year" "$mois"
done

# Pour 2016 2018 on prends tout les mois
for year in $(seq 2016 2020)
do
    for month in $(seq 1 12)
    do
        mois=$(printf %02d "$month")
        GetTripMonth "$year" "$mois"
    done
done
wait

GetStockIFSTTAR
wait

GetOpenBus
wait

GetGBFS

cd $initDir
