# Presentation of the Module
This is a module of visualisation for Bike Sharing New-York System Data.
Developed by Manuel Sulmont and Thomas Barzola
It is integrated in the [PhD of Thomas Barzola](LINKTOPHD)

# Usage of the module

## Download stock data from IFSTTAR and trip-data:
To download all the data use:

`./GetDataNY`

This script will download trips data from : <https://www.citibikenyc.com/system-data>  
and stocks data from  <http://vlsstats.ifsttar.fr/rawdata/RawData/RawData_OLD/>  
to a direcotry named 'Data_NY'.
We did not manage to download automatically the data from the Open Bus so please go to <https://www.theopenbus.com/raw-data.html> and ans store it to **_'Data_NY/Raw/Openbus'_**.

**'Data_NY' will be strutured like this:**  

    Data_NY  
    |──GBFS
    |──IFSTTAR
    |──Openbus
    |──Trips
    |──Raw
        |──GBFS
        |──IFSTTAR
        |──Openbus

## Convert the data to csv 
`./Convert.sh`:

This script will execute `./GetData_NY` and convert stock data from :

-   GBFS
-   IFSTTAR : <http://vlsstats.ifsttar.fr/rawdata/RawData/>
-   The Open Bus : <https://www.theopenbus.com/raw-data.html>

Stock data from The Open Bus and GBFS data must be placed  **_'Data_NY/Raw/Openbus'_** and **_'Data_NY/Raw/GBFS'_**, repectively.

To convert specifically a folder use the underlying python in `csvConversion/conversion.py Folder_To_Convert` or `csvConversion/location.py` to convert Trip data.

You can find a more precise description of this conversion module in `csvConversion/README.md`

## Using the visualisation

Go to `visualisation/index.html` to see all the visualisation we propose.
You must upload the files to visualize select the hour to start/stop the visualisation and the speed for dynamic visualisation.
Then by clicking on `submit` you will validate you parameter choice.
To choose other parameters you must revalidate with the `submit` button.
