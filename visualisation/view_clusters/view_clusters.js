var CSVDATA;
var STATION_LOCATION;

//todo variable selectionnabletime for time select;
// todo structure tripsurplace...
var map = L
  .map('map')
  .setView([40.752442, -73.985493], 12);
L.tileLayer('https://{s}.tile.jawg.io/jawg-dark/{z}/{x}/{y}{r}.png?access-token={accessToken}', {
  attribution: '<a href="http://jawg.io" title="Tiles Courtesy of Jawg Maps" target="_blank">&copy; <b>Jawg</b>Maps</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  minZoom: 10,
  maxZoom: 18,
  subdomains: 'abcd',
  accessToken: 'xhpfLgIVwKi5bqW9PxY4iWIBa1DsrqZ960zxnErordZjycgDr5a3IAqtJ7bLcFtO'
}).addTo(map);
L.svg().addTo(map);

// X and Y are vector parallels to streets of Manhattan
p1 = map.latLngToLayerPoint([40.736036, -73.993557]);
p2 = map.latLngToLayerPoint([40.798117, -73.948315]);
var X = p1.x - p2.x;
var Y = p1.y - p2.y;

// update position of stations when map is zoomed
map.on("zoomend", function() {
  // d3.selectAll("circle")
  //   .attr("cx", function(d) {
  //     return map.latLngToLayerPoint([getLocation(d).lat, getLocation(d).lon]).x;
  //   })
  //   .attr("cy", function(d) {
  //     return map.latLngToLayerPoint([getLocation(d).lat, getLocation(d).lon]).y;
  //   });

  d3.selectAll("path")
    .attr("d", function(d) {
      let p1 = map.latLngToLayerPoint([getLocation(d.start_id).lat, getLocation(d.start_id).lon]);
      let p2 = map.latLngToLayerPoint([getLocation(d.end_id).lat, getLocation(d.end_id).lon]);
      if (ANGLE) {
        let p3 = getAngle(p1, p2);
        return "M" + p1.x + "," + p1.y + "L" + p3.x + "," + p3.y + "L" + p2.x + "," + p2.y;
      } else {
        return "M" + p1.x + "," + p1.y + "L" + p2.x + "," + p2.y;
      }
    })
  // .attr("stroke-dasharray", function(d) {
  //   let a =       d3.select("#" + getPathId(d)).node().getTotalLength();
  //   return a;
  // })
  // .attr("stroke-dashoffset", function(d) {
  //   return d3.select("#" + getPathId(d)).node().getTotalLength();
  // });

  d3.selectAll("linearGradient")
    .attr("x1", function(d) {
      return map.latLngToLayerPoint([getLocation(d.start_id).lat, getLocation(d.start_id).lon]).x;
    })
    .attr("y1", function(d) {
      return map.latLngToLayerPoint([getLocation(d.start_id).lat, getLocation(d.start_id).lon]).y;
    })
    .attr("x2", function(d) {
      return map.latLngToLayerPoint([getLocation(d.end_id).lat, getLocation(d.end_id).lon]).x;
    })
    .attr("y2", function(d) {
      return map.latLngToLayerPoint([getLocation(d.end_id).lat, getLocation(d.end_id).lon]).y;
    });
});



function openFile(event) {
  //open csv file and convert
  let input = event.target;

  let reader = new FileReader();
  reader.onload = function() {
    let csv = reader.result;
    CSVDATA = d3.csvParse(csv);
    convertCsv();
  };
  reader.readAsText(input.files[0]);
};

function convertCsv() {
  let nb = 0;

  TRIPS = d3.map();
  STATION_LOCATION = d3.map();
  for (var i = 0; i < CSVDATA.length; i++) {
    id = CSVDATA[i]["stationId"];
    //save location of station
    if (!STATION_LOCATION.has(id)) {
      STATION_LOCATION.set(id, {
        "lat": CSVDATA[i]["latitude"],
        "lon": CSVDATA[i]["longitude"]
      });
    }
  }
  console.log(STATION_LOCATION);
}




function showClusters(maxiter = 1) {

  getClusters(maxiter);
  d3.selectAll("g").remove();
  // show stations on map : green: ok, red: error
  // stations is a map with station_id as key
  color = ["blue", "green", "red", "yellow", "orange", "purple", "magenta","brown"]
  let gsel = d3.select("#map")
    .select("svg")
    .selectAll("g")
    .data(CLUSTERSARRAY)
    .enter()
    .append("g")
    .attr("fill", function(d, i) {
      return color[i % color.length];
    })

  gsel.append("circle")
    .attr("cx", function(d, i) {
      return map.latLngToLayerPoint([CLUSTERS_LOCATION[i].lat, CLUSTERS_LOCATION[i].lon]).x;
    })
    .attr("cy", function(d, i) {
      return map.latLngToLayerPoint([CLUSTERS_LOCATION[i].lat, CLUSTERS_LOCATION[i].lon]).y;
    })
    .attr("stroke", "black")
    .attr("stroke-width", "3")
    .attr("r", "5");

  let lk = gsel.selectAll(".station")
    .data(function(d) {
      return d;
    })
    .enter()

  lk.append("line")
    .attr("x1", function(d) {
      return map.latLngToLayerPoint([CLUSTERS_LOCATION[CLUSTERS.get(d)].lat, CLUSTERS_LOCATION[CLUSTERS.get(d)].lon]).x;
    })
    .attr("y1", function(d) {
      return map.latLngToLayerPoint([CLUSTERS_LOCATION[CLUSTERS.get(d)].lat, CLUSTERS_LOCATION[CLUSTERS.get(d)].lon]).y;
    })
    .attr("x2", function(d) {
      return map.latLngToLayerPoint([STATION_LOCATION.get(d).lat, STATION_LOCATION.get(d).lon]).x;
    })
    .attr("y2", function(d) {
      return map.latLngToLayerPoint([STATION_LOCATION.get(d).lat, STATION_LOCATION.get(d).lon]).y;
    })
    .style("stroke-width", "2")
    .style("stroke", function(d) {
      return color[CLUSTERS.get(d) % color.length];
    });
  lk.append("circle")
    .attr("cx", function(d, i) {
      return map.latLngToLayerPoint([STATION_LOCATION.get(d).lat, STATION_LOCATION.get(d).lon]).x;
    })
    .attr("cy", function(d, i) {
      return map.latLngToLayerPoint([STATION_LOCATION.get(d).lat, STATION_LOCATION.get(d).lon]).y;
    })
    .attr("r", "3")
    .attr("class", "station");



  if (maxiter < 20) {
    setTimeout(function() {
      showClusters(maxiter + 1);
    }, 500);

  }
}
