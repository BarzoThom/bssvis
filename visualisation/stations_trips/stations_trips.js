var LOCATION;
var STARTDATA;
var ENDDATA;
var TIMES;
var CSVDATA;
var START = 0;
var END = 0;
var TYPE;

var map = L
  .map('map')
  .setView([40.752442, -73.985493], 12);
L.tileLayer('https://{s}.tile.jawg.io/jawg-dark/{z}/{x}/{y}{r}.png?access-token={accessToken}', {
  attribution: '<a href="http://jawg.io" title="Tiles Courtesy of Jawg Maps" target="_blank">&copy; <b>Jawg</b>Maps</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  minZoom: 10,
  maxZoom: 18,
  subdomains: 'abcd',
  accessToken: 'xhpfLgIVwKi5bqW9PxY4iWIBa1DsrqZ960zxnErordZjycgDr5a3IAqtJ7bLcFtO'
}).addTo(map);
L.svg().addTo(map);

// create a tooltip
var Tooltip = d3.select("#tooltip")
  .property("hidden", true)
  .attr("class", "tooltip")
  .style("background-color", "white")
  .style("border", "solid")
  .style("border-width", "2px")
  .style("border-radius", "5px")
  .style("padding", "5px")


function mouseover(d) {
  showOD(d);
  Tooltip
    .property("hidden", false)
    .style("left", (d3.mouse(this)[0] + 50) + "px")
    .style("top", (d3.mouse(this)[1]) + "px")
    .html(d);
}

function mouseleave(d) {
  clearOD();
  Tooltip
    .property("hidden", true);
}

// update position of stations when map is moving
map.on("zoomend", function() {
  d3.selectAll(".station")
    .attr("cx", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).x;
    })
    .attr("cy", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).y;
    });

  d3.selectAll("line")
    .attr("x2", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).x;
    })
    .attr("y2", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).y;
    })
});

function openFile(event) {
  //open csv file and convert
  let input = event.target;
  let reader = new FileReader();
  reader.onload = function() {
    loadData(reader.result);
  };
  reader.readAsText(input.files[0]);
};

function loadData(data){
  CSVDATA = d3.csvParseRows(data);
  getTimesAndLocationsFromCSV();
  setSelectTime();
  activateControls();
}

function getTimesAndLocationsFromCSV() {
  // get all available times from csv data
  // and initialise location of stations
  LOCATION = d3.map();
  TIMES = [];
  let start, end;
  tu = Date.now();
  for (var i = 1; i < CSVDATA.length; i++) {
    startId = CSVDATA[i][3];
    endId = CSVDATA[i][7];
    start = Date.parse(CSVDATA[i][1]);
    end = Date.parse(CSVDATA[i][2]);
    let j = TIMES.length - 1;
    while (j >= 0 && TIMES[j] > end) {
      j--;
    }
    if (j < 0 || TIMES[j] < end) {
      TIMES.splice(j + 1, 0, end);
    }
    while (j >= 0 && TIMES[j] > start) {
      j--;
    }
    if (j < 0 || TIMES[j] < start) {
      TIMES.splice(j + 1, 0, start);
    }
    if (!LOCATION.has(startId)) {
      LOCATION.set(startId, {
        "lat": CSVDATA[i][5],
        "lon": CSVDATA[i][6]
      });
    }
    if (!LOCATION.has(endId)) {
      LOCATION.set(endId, {
        "lat": CSVDATA[i][9],
        "lon": CSVDATA[i][10]
      });
    }
  }
}


function convertCsv() {
  // convert csv data to maps
  // STARTDATA contain for each departure station
  // a map containing for each arrivals tation
  // a number of trips between the two stations
  // ENDDATA contain the same structure but departure and arrivals are permuted
  STARTDATA = d3.map();
  ENDDATA = d3.map();
  let startId, endId, start, end;
  for (var i = 1; i < CSVDATA.length; i++) {
    startId = CSVDATA[i][3];
    endId = CSVDATA[i][7];
    start = Date.parse(CSVDATA[i][1]);
    end = Date.parse(CSVDATA[i][2]);
    if (start >= START && end <= END) {
      if (!STARTDATA.has(startId)) {
        STARTDATA.set(startId, d3.map());
      }
      if (!STARTDATA.get(startId).has(endId)) {
        STARTDATA.get(startId).set(endId, 0);
      }
      let n = STARTDATA.get(startId).get(endId) + 1;
      STARTDATA.get(startId).set(endId, n);
      if (!ENDDATA.has(endId)) {
        ENDDATA.set(endId, d3.map());
      }
      if (!ENDDATA.get(endId).has(startId)) {
        ENDDATA.get(endId).set(startId, 0);
      }
      n = ENDDATA.get(endId).get(startId) + 1;
      ENDDATA.get(endId).set(startId, n);
    }
  }
}


function showStations() {
  TYPE = document.querySelector('input[name="type"]:checked').value;
  var data;
  START = getTimeFromInputs();
  END = getTimeFromInputs(true);
  convertCsv();
  if (TYPE == "start") {
    data = STARTDATA;
  } else {
    data = ENDDATA;
  }

  let stations = LOCATION.keys();

  let nb = 0;
  let nbst = 0;
  for (var key of data.keys()) {
    nbst++;
    for (var v of data.get(key).values()) {
      nb += v;
    }
  }
  let ratio = nb / nbst;

  d3.select("#map")
    .select("svg")
    .selectAll("circle")
    .remove();

  let sel = d3.select("#map")
    .select("svg")
    .selectAll("circle")
    .data(stations)
    .enter()

  sel.append("circle")
    .attr("cx", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).x;
    })
    .attr("cy", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).y;
    })
    .attr("r", function(d) {
      let a = 0;
      if (data.has(d)) {
        for (var v of data.get(d).values()) {
          a += v;
        }
        // return 1+Math.sqrt(a/30);
        return a * 4 / ratio;
      } else {
        return 3;
      }
    })
    .attr("fill", "green")
    .attr("pointer-events", "visible")
    .attr("class", "station")
    .attr("stroke","darkgreen")
    .attr("stroke-width","1px")
    .on("mouseover", mouseover)
    .on("mouseleave", mouseleave)
}


function clearOD() {
  d3.select("#map")
    .select("svg")
    .selectAll("circle.end")
    .remove();

  d3.select("#map")
    .select("svg")
    .selectAll("line")
    .remove();

  //show all station
  d3.select("#map")
    .select("svg")
    .selectAll("circle.station")
    .style("fill", "green")
    .style("stroke", "darkgreen");
}

function showOD(stationId) {
  let total = 0;
  if (TYPE == "start") {
    data = STARTDATA;
  } else {
    data = ENDDATA;
  }
  if (!data.has(stationId)) {
    return;
  }
  let max = null;
  let nbst = 0;
  let min = null;
  for (var v of data.get(stationId).values()) {
    total += v;
    nbst++;
    if (max == null && min == null) {
      max = v;
      min = v;
    }
    if (max < v) {
      max = v;
    } else if (min > v) {
      min = v;
    }
  }

  let stations = data.get(stationId).keys();

  //hide all stations
  d3.select("#map")
    .select("svg")
    .selectAll("circle.station")
    .style("fill", "none")
    .style("stroke", "none");

  //add line
  d3.select("#map")
    .select("svg")
    .selectAll("line")
    .data(stations)
    .enter()
    .append("line")
    .attr("x1", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).x;
    })
    .attr("y1", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).y;
    })
    .attr("x2", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(stationId).lat, LOCATION.get(stationId).lon]).x;
    })
    .attr("y2", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(stationId).lat, LOCATION.get(stationId).lon]).y;
    })
    .style("stroke-width", function(d) {
      return data.get(stationId).get(d) * 5;
    })
    .style("stroke", "green");


  //add circles
  d3.select("#map")
    .select("svg")
    .selectAll("circle.end")
    .data(stations)
    .enter()
    .append("circle")
    .attr("cx", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).x;
    })
    .attr("cy", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).y;
    })
    .attr("r", function(d) {
      if (TYPE == "start") {
        nw_data = ENDDATA;
      } else {
        nw_data = STARTDATA;
      }
      let sum = 0;
      for (e of nw_data.get(d).keys()) {
        sum += nw_data.get(d).get(e);
      }
      return sum;
    })
    .attr("stroke","darkgreen")
    .attr("stroke-width","1px")
    .attr('class', "end")
    .style("fill", "green")
}
