var TRIPS;
var CSVDATA;
var TIMES;
var START;
var STARTANIMTIME;
var MAXTRIP = 700;
var TRIPSDISPLAYED;
var LASTDISPLAYEDTIME;
var NEXTDISPLAYEDTIME;
var UPDATETIMEOUT;
var CLOCKINTERVAL;
var STATION_LOCATION;
var ANGLE = true;
var SPEED = 4;


var map = L
  .map('map')
  .setView([40.752442, -73.985493], 12);
L.tileLayer('https://{s}.tile.jawg.io/jawg-dark/{z}/{x}/{y}{r}.png?access-token={accessToken}', {
  attribution: '<a href="http://jawg.io" title="Tiles Courtesy of Jawg Maps" target="_blank">&copy; <b>Jawg</b>Maps</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  minZoom: 10,
  maxZoom: 18,
  subdomains: 'abcd',
  accessToken: 'xhpfLgIVwKi5bqW9PxY4iWIBa1DsrqZ960zxnErordZjycgDr5a3IAqtJ7bLcFtO'
}).addTo(map);
L.svg().addTo(map);

// X and Y are vector parallels to streets of Manhattan
p1 = map.latLngToLayerPoint([40.736036, -73.993557]);
p2 = map.latLngToLayerPoint([40.798117, -73.948315]);
var X = p1.x - p2.x;
var Y = p1.y - p2.y;

// update position of stations when map is zoomed
map.on("zoomend", function() {
  d3.selectAll(".station")
    .attr("cx", function(d) {
      return map.latLngToLayerPoint([getLocation(d).lat, getLocation(d).lon]).x;
    })
    .attr("cy", function(d) {
      return map.latLngToLayerPoint([getLocation(d).lat, getLocation(d).lon]).y;
    });

  d3.selectAll("path")
    .attr("d", function(d) {
      let p1 = map.latLngToLayerPoint([getLocation(d.start_id).lat, getLocation(d.start_id).lon]);
      let p2 = map.latLngToLayerPoint([getLocation(d.end_id).lat, getLocation(d.end_id).lon]);
      if (ANGLE) {
        let p3 = getAngle(p1, p2);
        return "M" + p1.x + "," + p1.y + "L" + p3.x + "," + p3.y + "L" + p2.x + "," + p2.y;
      } else {
        return "M" + p1.x + "," + p1.y + "L" + p2.x + "," + p2.y;
      }
    })
  .attr("stroke-dasharray", function(d) {
    let a =       d3.select("#" + getPathId(d)).node().getTotalLength();
    return a;
  })
  .attr("stroke-dashoffset", function(d) {
    return d3.select("#" + getPathId(d)).node().getTotalLength();
  })
  .select("animate")
  .attr("values", function(d) {
    return d3.select("#" + getPathId(d)).node().getTotalLength() + ";0";
  })

  d3.selectAll("linearGradient")
    .attr("x1", function(d) {
      return map.latLngToLayerPoint([getLocation(d.start_id).lat, getLocation(d.start_id).lon]).x;
    })
    .attr("y1", function(d) {
      return map.latLngToLayerPoint([getLocation(d.start_id).lat, getLocation(d.start_id).lon]).y;
    })
    .attr("x2", function(d) {
      return map.latLngToLayerPoint([getLocation(d.end_id).lat, getLocation(d.end_id).lon]).x;
    })
    .attr("y2", function(d) {
      return map.latLngToLayerPoint([getLocation(d.end_id).lat, getLocation(d.end_id).lon]).y;
    });
});

function getAngle(p1, p2) {
  // get poitn to make an angle for trip prallels to street of Manhattan
  let a = (X * (p2.x - p1.x) + Y * (p2.y - p1.y)) / (X ** 2 + Y ** 2);
  return {
    x: (p1.x + a * X),
    y: (p1.y + a * Y)
  }
}

function getControlPoint(lat1, lon1, lat2, lon2) {
  let p1 = map.latLngToLayerPoint([lat1, lon1]);
  let p2 = map.latLngToLayerPoint([lat2, lon2]);
  let dist = Math.sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2);
  let a = fun(dist);
  return {
    x: (p1.x + p2.x) / 2 + a * (p2.y - p1.y) / dist,
    y: (p1.y + p2.y) / 2 - a * (p2.x - p1.x) / dist
  };
}

function fun(x) {
  //function used with quadratic curve
  if (x < 100) {
    return x / 50;
  }
  let a = 1 / 300; //1/4000
  let b = 0;
  let c = 0;
  // return a * x ** 2 + b * x + c;
  return 5 * Math.sqrt(x);
}


function openFile(event) {
  stopanim();
  //open csv file and convert
  let input = event.target;

  let reader = new FileReader();
  reader.onload = function() {
    loadData(reader.result);
  };
  reader.readAsText(input.files[0]);
};


function loadData(data) {
  CLUSTERIZE = false;
  document.getElementById('clusterize').checked = false;
  CSVDATA = d3.csvParseRows(data);
  convertCsv();
  activateControls();
  setSelectTime();
}

function convertCsv() {
  let nb = 0;
  if (!CLUSTERIZE) {
    TRIPS = d3.map();
    STATION_LOCATION = d3.map();
    for (var i = 1; i < CSVDATA.length; i++) {
      startId = CSVDATA[i][3];
      endId = CSVDATA[i][7];
      start = Date.parse(CSVDATA[i][1]);
      end = Date.parse(CSVDATA[i][2]);
      //save location of station
      if (!STATION_LOCATION.has(startId)) {
        STATION_LOCATION.set(startId, {
          "lat": CSVDATA[i][5],
          "lon": CSVDATA[i][6]
        });
      }
      if (!STATION_LOCATION.has(endId)) {
        STATION_LOCATION.set(endId, {
          "lat": CSVDATA[i][9],
          "lon": CSVDATA[i][10]
        });
      }
      if (!TRIPS.has(start)) {
        TRIPS.set(start, []);
      }
      TRIPS.get(start).push({
        start_id: startId,
        start: start,
        end_id: endId,
        end: end,
        nb: 1
      });
      nb++;
    }
  } else {
    TRIPS = d3.map();
    for (var i = 1; i < CSVDATA.length; i++) {
      startId = CLUSTERS.get(CSVDATA[i][3]);
      endId = CLUSTERS.get(CSVDATA[i][7]);
      start = Date.parse(CSVDATA[i][1]);
      end = Date.parse(CSVDATA[i][2]);
      start = Math.round(start / 300000) * 300000; //ruond to 5 min
      end = Math.round(end / 300000) * 300000;
      if (!TRIPS.has(start)) {
        TRIPS.set(start, []);
      }
      let found = false;
      for (var v of TRIPS.get(start)) {
        if (v.start_id == startId && v.end_id == endId && v.end == end) {
          v.nb++;
          found = true;
        }
      }
      if (!found) {
        TRIPS.get(start).push({
          start_id: startId,
          start: start,
          end_id: endId,
          end: end,
          nb: 1
        });
        nb++;
      }
    }
  }
  TIMES = TRIPS.keys().sort();
}


function getTimeout() {
  return (NEXTDISPLAYEDTIME - START) / SPEED * 1000 - (Date.now() - STARTANIMTIME);
}


function activateControls() {
  //activate "submit", "play" and "pause" buttons
  buttons = document.getElementsByClassName("control");
  for (let b of buttons) {
    b.disabled = false;
  }
}


function getGradId(d) {
  return "grad_" + d.start + d.start_id + d.end_id + d.end;
}

function getPathId(d) {
  return "path_" + d.start + d.start_id + d.end_id + d.end;
}

function getCircleId(d) {
  return "circle_" + d.start + d.start_id + d.end_id + d.end;
}

function startanim() {
  stopanim();
  document.getElementById("stopButton").disabled = false;
  document.getElementById("startButton").disabled = true;
  START = getTimeFromInputs();
  SPEED = document.getElementById("speed").value * 100000;

  if (CLUSTERIZE) {
    getClusters();
  }
  convertCsv();
  showStations();
  TRIPSDISPLAYED = null;
  getTripsToDisplay();
  showTrips();
  STARTANIMTIME = Date.now();
  UPDATETIMEOUT = setTimeout(function() {
    updateTrips();
  }, getTimeout());
  CLOCKINTERVAL = setInterval(function() {
    updateClock();
  }, 250);
  document.getElementById("starter").beginElement();
}

function stopanim() {
  document.getElementById("stopButton").disabled = true;
  document.getElementById("startButton").disabled = false;
  clearTimeout(UPDATETIMEOUT);
  clearInterval(CLOCKINTERVAL);
  d3.select("#map")
    .select("svg")
    .selectAll("*")
    .remove();

  d3.select("#map")
    .select("svg")
    .append("animate")
    .attr("id", "starter")
    .attr("begin", "indefinite");
}

function updateClock() {
  let time = Date.now();
  time = START + (time - STARTANIMTIME) * SPEED / 1000;
  printTime(time);
}


function getAnimBegin(d) {
  return (d.start - START) / SPEED;

}

function getAnimDur(d) {
  return (d.end - d.start) / SPEED;
}

function getTripsToDisplay() {
  let nbtrip = 0;
  let i = 0;
  NEXTDISPLAYEDTIME = null;
  if (!TRIPSDISPLAYED) { //ANIM N'AS PAS COMMENCÉ
    TRIPSDISPLAYED = [];
    LASTDISPLAYEDTIME = START;
    i = 0;
  } else {
    i = TIMES.indexOf(NEXTDISPLAYEDTIME);
  }



  while (i < TIMES.length && !NEXTDISPLAYEDTIME) {
    if (TIMES[i] > LASTDISPLAYEDTIME && TRIPSDISPLAYED.length < MAXTRIP) {
      TRIPSDISPLAYED = TRIPSDISPLAYED.concat(TRIPS.get(TIMES[i]));
      LASTDISPLAYEDTIME = TIMES[i];
    } else if (TIMES[i] > LASTDISPLAYEDTIME && TRIPSDISPLAYED.length >= MAXTRIP && !NEXTDISPLAYEDTIME) {
      NEXTDISPLAYEDTIME = TIMES[i];
    }
    i++;
  }
}

function updateTrips() {
  // t1 = time;
  // console.log("optim");
  removeOldTrips();
  // t2 = Date.now();
  // console.log("deltat removelold : " + (t2-t1));
  getTripsToDisplay();
  // t1 = Date.now();
  // console.log("deltat gettrip : " + (t1-t2));
  showTrips();
  // console.log("deltat addnextanim : " + (Date.now()-t1));

  if (NEXTDISPLAYEDTIME) {
    UPDATETIMEOUT = setTimeout(function() {
        updateTrips();
      },
      getTimeout());
  }
}


function removeOldTrips() {
  let time = Date.now();

  let i = 0;
  let d;

  let sel = d3.select("#map")
    .select("svg")
    .selectAll(".trip")

  sel.each(function(d) {

    if (STARTANIMTIME + ((d.end - START) / SPEED) * 1000 < time - 1000) {
      i = TRIPSDISPLAYED.indexOf(d);
      if (i != -1) {
        TRIPSDISPLAYED.splice(i, 1);
      }
      this.remove();
    }
  });
}

function showTrips() {

  var nb = 0;
  let sel = d3.select("#map")
    .select("svg")
    .selectAll(".trip")
    .data(TRIPSDISPLAYED)
    .enter()
    .append("g")
    .attr("class", "trip");


  grad = sel.append('linearGradient')
    .attr("id", function(d) {
      nb++;
      return getGradId(d)
    })
    .attr("gradientUnits", "userSpaceOnUse")
    .attr("x1", function(d) {

      // return 0;
      return map.latLngToLayerPoint([getLocation(d.start_id).lat, getLocation(d.start_id).lon]).x;

    })
    .attr("y1", function(d) {
      // return 0;
      return map.latLngToLayerPoint([getLocation(d.start_id).lat, getLocation(d.start_id).lon]).y;
    })
    .attr("x2", function(d) {
      return map.latLngToLayerPoint([getLocation(d.end_id).lat, getLocation(d.end_id).lon]).x;
    })
    .attr("y2", function(d) {
      return map.latLngToLayerPoint([getLocation(d.end_id).lat, getLocation(d.end_id).lon]).y;
    })

  grad.append("stop")
    .attr("offset", "0%")
    .attr("stop-color", "red");
  grad.append("stop")
    .attr("offset", "100%")
    .attr("stop-color", "blue");

  path = sel.append("path")
    .attr("id", function(d) {
      return getPathId(d)
    })
    .attr("d", function(d) {
      p1 = map.latLngToLayerPoint([getLocation(d.start_id).lat, getLocation(d.start_id).lon]);
      p2 = map.latLngToLayerPoint([getLocation(d.end_id).lat, getLocation(d.end_id).lon]);
      if (ANGLE) {
        c = getAngle(p1, p2);
        return "M" + p1.x + "," + p1.y + "L" + c.x + "," + c.y + "L" + p2.x + "," + p2.y;

      } else {
        return "M" + p1.x + "," + p1.y + "L" + p2.x + "," + p2.y;
      }
    })
    .style("stroke-width", function(d) {
      return 1.5 * d.nb;
    })
    .attr("stroke", function(d) {
      return "url(#" + getGradId(d) + ")";
    })
    .style("fill", "none")
    .attr("stroke-dasharray", function(d) {
      return d3.select("#" + getPathId(d)).node().getTotalLength();
    })
    .attr("stroke-dashoffset", function(d) {
      return d3.select("#" + getPathId(d)).node().getTotalLength();
    })
    .append("animate")
    .attr("attributeName", "stroke-dashoffset")
    .attr("values", function(d) {
      return d3.select("#" + getPathId(d)).node().getTotalLength() + ";0";
    })
    .attr("begin", function(d) {
      return "starter.begin+" + getAnimBegin(d) + "s";
    })
    .attr("dur", function(d) {
      return getAnimDur(d) + "s";
    })



  sel.append("circle")
    .attr("id", function(d) {
      return getCircleId(d);
    })
    .attr("r", function(d) {
      if (d.start_id == d.end_id) {
        return 2 * d.nb;

      } else {
        return 0.75 * d.nb;;
      }
    })
    .attr("fill", "orange")
    // .attr("class", "trip")
    .append("animateMotion")
    .attr("class", "animator")
    // .attr("repeatCount","indefinite")
    // .attr("begin","indefinite")
    .attr("begin", function(d) {
      return "starter.begin+" + getAnimBegin(d) + "s";
    })
    .attr("dur", function(d) {
      return getAnimDur(d) + "s";
    })
    // .attr("fill", "freeze")
    .append("mpath")
    .attr("xlink:href", function(d) {
      return "#" + getPathId(d);
    })
}




function showStations() {
  // show stations on map : green: ok, red: error
  // stations is a map with station_id as key
  d3.select("#map")
    .select("svg")
    .selectAll(".station")
    .data(function() {
      if (!CLUSTERIZE) {
        return STATION_LOCATION.keys();
      } else {
        return CLUSTERS_LOCATION.keys();
      }
    })
    .enter()
    .append("circle")
    .attr("id", function(d) {
      return "station_" + d;
    })
    .attr("cx", function(d) {
      return map.latLngToLayerPoint([getLocation(d).lat, getLocation(d).lon]).x;
    })
    .attr("cy", function(d) {
      return map.latLngToLayerPoint([getLocation(d).lat, getLocation(d).lon]).y;
    })
    .attr("r", 2)
    .attr("fill", "green")
    .attr("class", "station")
}

//todo : var triponplace?

function activateAngle(e) {
  ANGLE = !e.checked;
}
