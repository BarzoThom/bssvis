var TIMES;
var TIMEINDEX;
var CURRENTTIME;
var INTERVAL;
var EVENEMENTS;
var STOCKDATA = null;
var TRIPDATA = null;
var RELOCDATA = null;
var DATA;
var SELSTATION;
var GRAPHSCALEX;
var ANIM = false;
var LOCATION;
var STATIONS;

//create map layer
var map = L
  .map('map')
  .setView([40.752442, -73.985493], 12);
L.tileLayer('https://{s}.tile.jawg.io/jawg-dark/{z}/{x}/{y}{r}.png?access-token={accessToken}', {
  attribution: '<a href="http://jawg.io" title="Tiles Courtesy of Jawg Maps" target="_blank">&copy; <b>Jawg</b>Maps</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  minZoom: 10,
  maxZoom: 18,
  subdomains: 'abcd',
  accessToken: 'xhpfLgIVwKi5bqW9PxY4iWIBa1DsrqZ960zxnErordZjycgDr5a3IAqtJ7bLcFtO'
}).addTo(map);
L.svg().addTo(map);

// update position of STATIONS (circles) when map is moving
map.on("zoomend", function() {
  d3.selectAll("circle")
    .attr("cx", function(d) {
      return map.latLngToLayerPoint([getLocation(d).lat, getLocation(d).lon]).x;
    })
    .attr("cy", function(d) {
      return map.latLngToLayerPoint([getLocation(d).lat, getLocation(d).lon]).y;
    });
});

// create a tooltip
var Tooltip = d3.select("#tooltip")
  .property("hidden", true)
  .attr("class", "tooltip")
  .style("background-color", "white")
  .style("border", "solid")
  .style("border-width", "2px")
  .style("border-radius", "5px")
  .style("padding", "5px")

function mouseover(d) {
  SELSTATION = d;
  let date = new Date(parseInt(CURRENTTIME))
  let start = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime();
  let end = start + 86400000;
  let data = [];
  let cap = STATIONS.get(d).capacity;
  let i = 0
  while (i < TIMES.length && TIMES[i] < end) {
    if (TIMES[i] >= start) {
      data.push({
        time: new Date(parseInt(TIMES[i])),
        stock_s: DATA.get(TIMES[i]).get(d).stock_s,
        stock_t: DATA.get(TIMES[i]).get(d).stock_t
      });
    }
    i++;
  }

  let margin = {
    top: 10,
    right: 30,
    bottom: 30,
    left: 60
  };
  let width = 400 - margin.left - margin.right;
  let height = 300 - margin.top - margin.bottom;

  var svgGraph = d3.select("#graph")
    .property("hidden", false)
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
      "translate(" + margin.left + "," + margin.top + ")");

  //add X axis
  GRAPHSCALEX = d3.scaleTime()
    .domain(d3.extent(data, function(d) {
      return d.time;
    }))
    .range([0, width])
    .nice();
  svgGraph.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(GRAPHSCALEX));


  // Add Y axis
  var y = d3.scaleLinear()
    .domain([0, cap])
    .range([height, 0]);
  svgGraph.append("g")
    .call(d3.axisLeft(y));

  // Add the line
  svgGraph.append("path")
    .datum(data)
    .attr("fill", "none")
    .attr("stroke", "steelblue")
    .attr("stroke-width", 1.5)
    .attr("d", d3.line()
      .x(function(d) {
        return GRAPHSCALEX(d.time)
      })
      .y(function(d) {
        return y(d.stock_s)
      })
    )

  // Add the line
  svgGraph.append("path")
    .datum(data)
    .attr("fill", "none")
    .attr("stroke", "darkred")
    .attr("stroke-width", 1.5)
    .attr("d", d3.line()
      .x(function(d) {
        return GRAPHSCALEX(d.time)
      })
      .y(function(d) {
        return y(d.stock_t)
      })
    )

  // Add vertical line to plot
  svgGraph.append("path")
    .attr("fill", "none")
    .attr("stroke", "yellow")
    .attr("stroke-width", 1.5)
    .attr("d", "M" + GRAPHSCALEX(CURRENTTIME) + ",0v" + height)
    .attr("id", "verticalLine")


}

function mouseleave(d) {
  Tooltip.property("hidden", true);
  d3.select("#graph *").remove();
  d3.select("#graph").property("hidden", true);
}


function openStockFile(event) {
  stopAnim();
  //open csv file and convert
  let input = event.target;

  let reader = new FileReader();
  reader.onload = function() {
    STOCKDATA = reader.result;
    loadData(STOCKDATA, TRIPDATA);
  };
  reader.readAsText(input.files[0]);
};

function openTripFile(event) {
  stopAnim();
  //open csv file and convert
  let input = event.target;

  let reader = new FileReader();
  reader.onload = function() {
    TRIPDATA = reader.result;
    loadData(STOCKDATA, TRIPDATA);
  };
  reader.readAsText(input.files[0]);
};

function loadData(stock, trip, reloc = false) {
  if (reloc) {
    RELOCDATA = 0;
  }
  if (stock && trip && RELOCDATA != null) {
    stopAnim();
    STOCKDATA = d3.csvParse(stock);
    TRIPDATA = d3.csvParseRows(trip);
    convertCsv();
    setSelectTime();
    activateControls();
  }
}

function openRelocFile(event) {
  stopAnim();
  //open csv file and convert
  let input = event.target;

  let reader = new FileReader();
  reader.onload = function() {
    let json = reader.result;
    RELOCDATA = JSON.parse(json);
    setRelocation();
  };
  reader.readAsText(input.files[0]);
};

function checkReloc(e) {
  document.getElementById('relocFileInput').disabled = !e.checked;
  if (e.checked) {
    RELOCDATA = null;
  } else {
    RELOCDATA = 0;
  }
  STOCKDATA = null;
  TRIPDATA = null;
  document.getElementById('tripFileInput').value = null;
  document.getElementById('stockFileInput').value = null;
}

function setRelocation() {
  EVENEMENTS = d3.map();
  for (var key of RELOCDATA.keys()) {
    let time = key[0];
    let station = key[1];
    addEvenement(station, time, RELOCDATA.get(key));
  }
}

function addEvenement(station, time, value) {
  if (!EVENEMENTS.has(station)) {
    EVENEMENTS.set(station, d3.map());
  }
  if (EVENEMENTS.get(station).has(time)) {
    value += EVENEMENTS.get(station).get(time);
  }
  EVENEMENTS.get(station).set(time, value);
}

function getEvenements(station, time) {
  let v = 0;
  if (EVENEMENTS.has(station)) {
    for (var key of EVENEMENTS.get(station).keys()) {
      if (key <= time) {
        v += EVENEMENTS.get(station).get(key);
        EVENEMENTS.get(station).remove(key);
      }
    }
    return v;
  } else {
    return 0;
  }
}

function convertCsv() {
  /*
  Convert csvarray to map with :
      (key: timestamp
      value: map(
              key : id_station
              value : {
                stock_t,
                Stock_s,
                Capacity
              }))
  */
  if (!EVENEMENTS) {
    EVENEMENTS = d3.map()
  }
  for (var i = 1; i < TRIPDATA.length; i++) {
    startId = TRIPDATA[i][3];
    endId = TRIPDATA[i][7];
    start = Date.parse(TRIPDATA[i][1]);
    end = Date.parse(TRIPDATA[i][2]);
    addEvenement(startId, start, -1);
    addEvenement(endId, end, 1);
  }

  DATA = d3.map();
  let initialStock = d3.map();
  LOCATION = d3.map(); // called in html
  let t;
  for (var i = 1; i < STOCKDATA.length; i++) {
    t = Date.parse(STOCKDATA[i].time);
    station = STOCKDATA[i].stationId;
    if (!DATA.has(t)) {
      //create map for time t
      DATA.set(t, d3.map());
    }
    if (!initialStock.has(station)) {
      getEvenements(station, t);
      initialStock.set(station, parseInt(STOCKDATA[i].nbBikes));
    }
    let stock_t = initialStock.get(station);
    stock_t += getEvenements(station, t);
    initialStock.set(station, stock_t);
    DATA.get(t).set(station, {
      stock_s: parseInt(STOCKDATA[i].nbBikes),
      stock_t: stock_t,
      capacity: parseInt(STOCKDATA[i].capacity)
    });

    if (!LOCATION.has(station)) {
      LOCATION.set(station, {
        "lat": STOCKDATA[i].latitude,
        "lon": STOCKDATA[i].longitude
      });
    }

  }

  TIMES = DATA.keys().sort();
}



function submit() {
  stopAnim();
  let time = getTimeFromInputs();
  TIMEINDEX = getNearestTime(TIMES, time);
  CURRENTTIME = TIMES[TIMEINDEX];
  printTime(CURRENTTIME);
  document.getElementById("startButton").disabled = false;
  showStations();
}


function showStations() {
  // show STATIONS on map at a specific time
  STATIONS = DATA.get(TIMES[TIMEINDEX])
  if (STATIONS == null) {
    stopAnim();
    return;
  }

  dat = STATIONS.keys();

  if (d3.select("#map")
    .select("svg").selectAll("circle").empty()) {

    d3.select("#map")
      .select("svg")
      .append("g")
      .attr("id", "capCircles")
  }

  let sel = d3.select("#capCircles")
    .selectAll("circle")
    .data(dat);

  sel.attr("r", function(d) { //size of outer circle linked to capacity
      let s = STATIONS.get(d);
      let r = (s.stock_s - s.stock_t) / 2;
      return (r != 0 ? (r > 0 ? 3 + r : 3 - r) : 3);
    })
    // .attr("pointer-events", "visible")
    // .on("mouseover", mouseover)
    // .on("mouseleave", mouseleave)
    // .on("mousemove", function() {
    //   Tooltip.style("top", (event.pageY) + "px").style("left", (event.pageX - 240) + "px");
    // })
    .style("fill", function(d) { // color of points linked to nbBikes/capacity
      let s = STATIONS.get(d);
      let r = s.stock_s - s.stock_t;
      if (r > 0) {
        return "blue";
      } else if (r < 0) {
        return "red";
      } else {
        return "green";
      }
    })

  sel.enter()
    .append("circle")
    .attr("cx", function(d) {
      return map.latLngToLayerPoint([getLocation(d).lat, getLocation(d).lon]).x
    })
    .attr("cy", function(d) {
      return map.latLngToLayerPoint([getLocation(d).lat, getLocation(d).lon]).y
    })
    .attr("r", function(d) { //size of outer circle linked to capacity
      let s = STATIONS.get(d);
      let r = (s.stock_s - s.stock_t) / 2;
      return (r != 0 ? (r > 0 ? 3 + r : 3 - r) : 3);
    })
    .attr("pointer-events", "visible")
    .on("mouseover", mouseover)
    .on("mouseleave", mouseleave)
    // .on("mousemove", function() {
    //   Tooltip.style("top", (event.pageY) + "px").style("left", (event.pageX - 240) + "px");
    // })
    .style("fill", function(d) { // color of points linked to nbBikes/capacity
      let s = STATIONS.get(d);
      let r = s.stock_s - s.stock_t;
      if (r > 0) {
        return "blue";
      } else if (r < 0) {
        return "red";
      } else {
        return "green";
      }
    })
}



function getLocation(d) {
  if (LOCATION.has(d)) {
    return LOCATION.get(d);
  } else {
    console.log("no location for :");
    console.log(d);
    return {
      lat: 40.7,
      lon: -73.9
    };
  }
}



function startAnim() {
  ANIM = true;
  document.getElementById("stopButton").disabled = false;
  document.getElementById("startButton").disabled = true;
  let speed = document.getElementById("speed").value;
  INTERVAL = setInterval(function() {
    anim();
  }, 1000 / speed);
}

function stopAnim() {
  if (ANIM) {
    document.getElementById("stopButton").disabled = true;
    document.getElementById("startButton").disabled = false;
    if (INTERVAL !== null) {
      window.clearInterval(INTERVAL);
      INTERVAL = null;
    }
  }
}

function anim() {
  nextTime();
  showStations();
  if (SELSTATION) {
    updateVerticalLine()
  }
}
