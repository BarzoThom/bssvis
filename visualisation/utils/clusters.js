var CLUSTERS;
var CLUSTERSARRAY;
var CLUSTERS_LOCATION;
var NBCLUSTERS = 10;
var CLUSTERIZE = false;

function dist(p1, p2) {
  // return distance between p1 and p2 using haversine.
  let radlat1 = p1.lat / 57.29577951;
  let radlon1 = p1.lon / 57.29577951;
  let radlat2 = p2.lat / 57.29577951;
  let radlon2 = p2.lon / 57.29577951;
  let a = 2 * 6371 * Math.asin(Math.sqrt(Math.sin((radlat2 - radlat1) / 2) ** 2 + Math.cos(radlat1) * Math.cos(radlat2) * Math.sin((radlon2 - radlon1) / 2) ** 2));
  return a;
}


function getBarycenter(stations) {
  // return barycenter of cluster of stations
  let sumLat = 0;
  let sumLon = 0;
  for (var s of stations) {
    sumLat += parseFloat(STATION_LOCATION.get(s).lat);
    sumLon += parseFloat(STATION_LOCATION.get(s).lon);
  }
  return {
    lat: sumLat / stations.length,
    lon: sumLon / stations.length
  };
}


function getLocation(s) {
  // return location of station or location of cluster
  if (CLUSTERIZE) {
    return CLUSTERS_LOCATION[s];
  } else {
    return STATION_LOCATION.get(s);
  }
}


function activateCluster(e) {
  CLUSTERIZE = e.checked;
  document.getElementById('nbClusters').disabled = !CLUSTERIZE;
}

function getClusters(maxiter = 20) {
  // k-means clustering of stations
  let te = Date.now();
  NBCLUSTERS = document.getElementById("nbClusters").value;
  let stations = STATION_LOCATION.keys();
  let error;
  let oldError = 0;
  let clustersLocation = new Array(NBCLUSTERS);
  for (var i = 0; i < NBCLUSTERS; i++) {
    clustersLocation[i] = STATION_LOCATION.get(stations[i]);
  }
  let clusters;
  let oldClusters;
  let oldClustersLocation
  k = 0;
  do {
    oldError = error;
    oldClusters = clusters;
    oldClustersLocation = clustersLocation;
    clusters = new Array(NBCLUSTERS);
    for (var i = 0; i < NBCLUSTERS; i++) {
      clusters[i] = [];
    }
    for (var station of stations) {
      clusters[getNearestCluster(station, oldClustersLocation)].push(station);
    }
    clustersLocation = getClustersLocation(clusters);
    error = getError(clusters, clustersLocation);
    k++;
  } while (k < maxiter && error != oldError);

  CLUSTERS_LOCATION = clustersLocation;
  CLUSTERSARRAY = clusters;
  CLUSTERS = d3.map();
  for (var i = 0; i < clusters.length; i++) {
    for (var s of clusters[i]) {
      CLUSTERS.set(s, i);
    }
  }
}


function getNearestCluster(station, clusters) {
  let stloc = STATION_LOCATION.get(station);
  let res = 0;
  let D = dist(stloc, clusters[0]);
  let d;
  for (var i = 1; i < NBCLUSTERS; i++) {
    d = dist(stloc, clusters[i]);
    if (d < D) {
      res = i;
      D = d;
    }
  }
  return res;
}

function getClustersLocation(clusters) {
  cluloc = new Array(NBCLUSTERS);
  for (var i = 0; i < NBCLUSTERS; i++) {
    cluloc[i] = getBarycenter(clusters[i]);
  }
  return cluloc;
}

function getError(clusters, clustersLocation) {
  let e = 0;
  for (var i = 0; i < clusters.length; i++) {
    for (var j = 0; j < clusters[i].length; j++) {
      e += dist(STATION_LOCATION.get(clusters[i][j]), clustersLocation[i]);
    }
  }
  return e;
}
