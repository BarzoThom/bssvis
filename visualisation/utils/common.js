function openControls() {
  // open left control panel
  document.getElementById("controls").style.left = "0";
  document.getElementById("map").style.marginLeft = "250px";
  document.getElementById("map").style.width = "calc(100% - 250px)";
  document.getElementById("closeControls").style.left = "250px";
  document.getElementById("closeControls").style.opacity = "1";
}

function closeControls() {
  // close left control panel
  document.getElementById("controls").style.left = "-260px";
  document.getElementById("map").style.marginLeft = "0";
  document.getElementById("map").style.width = "100%";
  document.getElementById("closeControls").style.left = "0";
  document.getElementById("closeControls").style.opacity = "0";
}


function activateControls() {
  //activate "submit", "play" and "pause" buttons
  buttons = document.getElementsByClassName("control");
  for (let b of buttons) {
    b.disabled = false;
  }
}

function printError(e) {
  document.getElementById("errorMessages").hidden = false;
  document.getElementById("errorMessages").innerHTML = e;
}

function clearError() {
  document.getElementById("errorMessages").hidden = true;
}

function getTimeFromInputs(end = false) {
  // return time in milliseconds from html inputs
  let c = "";
  if (end) {
    c = "E";
  }
  let e = document.getElementById(c + "hourInput")
  let hour = e.options[e.selectedIndex].value;
  e = document.getElementById(c + "minInput")
  let min = e.options[e.selectedIndex].value;
  e = document.getElementById(c + "dateInput")
  let time = e.options[e.selectedIndex].value;
  clearError();

  if (!isNaN(hour) && hour >= 0 && hour <= 23 && !isNaN(min) && min >= 0 && min <= 59) {
    time += " " + hour + ":" + min;
    time = Date.parse(time);

    if (isNaN(time)) {
      printError("Invalid Input");
      return null;
    }
    return Math.floor(time / 300000) * 300000; //a supprimer
  } else {
    printError("Invalid Input");
    return null;
  }
}

function setSelectTime(type = 0, end = false) {
  // fill html selects for time with options for each available data
  // type : 0 : day, 1 : hour, 2 : min
  let selectDate, node, date, hour, day, e,
    selectedDay = 0,
    selectedHour = 0,
    selectedStartDay = 0,
    selectedStartHour = 0,
    selectedStartMin = 0;
  let c = "";
  if (end) {
    c = "E";
  }
  if (type >= 1) {
    e = document.getElementById(c + "dateInput");
    selectedDay = e.options[e.selectedIndex].value;
    if (type == 2) {
      e = document.getElementById(c + "hourInput");
      selectedHour = e.options[e.selectedIndex].value;
    }
  }
  if (end) {
    e = document.getElementById("dateInput");
    selectedStartDay = e.options[e.selectedIndex].value;
    if (type >= 1) {
      e = document.getElementById("hourInput");
      selectedStartHour = e.options[e.selectedIndex].value;
      if (type == 2) {
        e = document.getElementById("minInput");
        selectedStartMin = e.options[e.selectedIndex].value;
      }
    }
  }
  switch (type) {
    case 1:
      select = document.getElementById(c + "hourInput");
      select.innerHTML = "";
      for (t of TIMES) {
        date = new Date(parseInt(t));
        day = date.toISOString().substring(0,10);
        hour = date.getHours();
        if (day == selectedDay && (!end || day > selectedStartDay || hour >= selectedStartHour) && document.getElementById(c + "hour_" + hour) == null) {
          node = new Option(hour, hour);
          node.id = c + "hour_" + hour;
          select.appendChild(node);
        }
      }
      break;
    case 2:
      select = document.getElementById(c + "minInput");
      select.innerHTML = "";
      for (t of TIMES) {
        date = new Date(parseInt(t));
        day = date.toISOString().substring(0,10);
        hour = date.getHours();
        min = date.getMinutes();
        if (day == selectedDay && hour == selectedHour && (!end || day > selectedStartDay || hour > selectedStartHour || min >= selectedStartMin) && document.getElementById(c + "min_" + min) == null) {
          node = new Option(min, min);
          node.id = c + "min_" + min;
          select.appendChild(node);
        }
      }
      break;
    default:
      if (typeof PUBLIC == 'undefined') {
        select = document.getElementById(c + "dateInput");
        select.innerHTML = "";
        for (t of TIMES) {
          date = new Date(parseInt(t));
          day = date.toISOString().substring(0,10);
          if ((!end || day >= selectedStartDay) && document.getElementById(c + "date_" + day) == null) {
            node = new Option(day, day);
            node.id = c + "date_" + day;
            select.appendChild(node);
          }
        }
      }

  }
  if (type < 2) {
    setSelectTime(type + 1, end)
  } else if (!end && document.getElementById("EdateInput")) {
    setSelectTime(0, true);
  }
}

function getNearestTime(times, time) {
  // return nearest time from an array of times
  let i = 0
  while (i < times.length && times[i] < time) {
    i++;
  }
  return i;
}

function printTime(time) {
  // show time in html div
  let d = new Date(parseInt(time));
  d3.select("#timeText")
    .html(d.toISOString().substring(0,10) + " " + d.getHours() + ":" + d.getMinutes());
}

function nextTime() {
  // set variable with the next value in array of times
  TIMEINDEX++;
  if (TIMEINDEX >= TIMES.length) {
    stopAnim();
    let time = getTimeFromInputs();
    TIMEINDEX = getNearestTime(TIMES, time);
    CURRENTTIME = TIMES[TIMEINDEX];
    return;
  }
  CURRENTTIME = TIMES[TIMEINDEX];
  printTime(CURRENTTIME);
}

function updateVerticalLine() {
  // update vertical line on the graph for stocks of the day
  let height = 260;
  d3.select("#verticalLine")
    .attr("d", "M" + GRAPHSCALEX(CURRENTTIME) + ",0v" + height)
}
