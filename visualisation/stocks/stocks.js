var STOCKS;
var TIMEINDEX;
var CURRENTTIME;
var TIMES;
var INTERVAL;
var STATIONS;
var SELSTATION;
var ANIM = false;
var LOCATION = d3.map();
var GRAPHSCALEX;


//create map layer
var map = L
  .map('map')
  .setView([40.752442, -73.985493], 12);
L.tileLayer('https://{s}.tile.jawg.io/jawg-light/{z}/{x}/{y}{r}.png?access-token={accessToken}', {
  attribution: '<a href="http://jawg.io" title="Tiles Courtesy of Jawg Maps" target="_blank">&copy; <b>Jawg</b>Maps</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  minZoom: 10,
  maxZoom: 18,
  subdomains: 'abcd',
  accessToken: 'xhpfLgIVwKi5bqW9PxY4iWIBa1DsrqZ960zxnErordZjycgDr5a3IAqtJ7bLcFtO'
}).addTo(map);
L.svg().addTo(map);

// update position of stations (circles) when map is moving
map.on("zoomend", function() {
  d3.selectAll("circle")
    .attr("cx", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).x;
    })
    .attr("cy", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).y;
    });
});

// create a tooltip
var Tooltip = d3.select("#tooltip")
  .property("hidden", true)
  .attr("class", "tooltip")
  .style("background-color", "white")
  .style("border", "solid")
  .style("border-width", "2px")
  .style("border-radius", "5px")
  .style("padding", "5px")

function mouseover(d) {
  SELSTATION = d;
  Tooltip
    .property("hidden", false)
    .style("left", (d3.mouse(this)[0] + 50) + "px")
    .style("top", d3.mouse(this)[0] + "px")
    .html(d + "<br> Bikes: " + STATIONS.get(d).nbBike +
      "<br> Places: " + STATIONS.get(d).nbPlace +
      "<br> Capacity: " + STATIONS.get(d).capacity);

  let date = new Date(parseInt(CURRENTTIME));
  let start = new Date(date.getFullYear(), date.getMonth(), date.getDate()).getTime();
  let end = start + 86400000;
  let data = [];
  let cap = STATIONS.get(d).capacity;
  let i = 0;
  while (i < TIMES.length && TIMES[i] <= end) {
    if (TIMES[i] >= start) {
      data.push({
        time: new Date(parseInt(TIMES[i])),
        value: STOCKS.get(TIMES[i]).get(d).nbBike
      });
    }
    i++;
  }

  let margin = {
    top: 10,
    right: 30,
    bottom: 30,
    left: 60
  };
  let width = 400 - margin.left - margin.right;
  let height = 300 - margin.top - margin.bottom;

  var svgGraph = d3.select("#graph")
    .property("hidden", false)
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform",
      "translate(" + margin.left + "," + margin.top + ")");

  // Add X axis
  GRAPHSCALEX = d3.scaleTime()
    .domain(d3.extent(data, function(d) {
      return d.time;
    }))
    .range([0, width])
    .nice();
  svgGraph.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(GRAPHSCALEX));

  // Add Y axis
  var y = d3.scaleLinear()
    .domain([0, cap])
    .range([height, 0]);
  svgGraph.append("g")
    .call(d3.axisLeft(y));

  // Add the line
  svgGraph.append("path")
    .datum(data)
    .attr("fill", "none")
    .attr("stroke", "steelblue")
    .attr("stroke-width", 1.5)
    .attr("d", d3.line()
      .x(function(d) {
        return GRAPHSCALEX(d.time)
      })
      .y(function(d) {
        return y(d.value)
      })
    )

  // Add vertical line to plot
  svgGraph.append("path")
    .attr("fill", "none")
    .attr("stroke", "yellow")
    .attr("stroke-width", 1.5)
    .attr("d", "M" + GRAPHSCALEX(CURRENTTIME) + ",0v" + height)
    .attr("id", "verticalLine")
}

function mouseleave(d) {
  SELSTATION = null;
  Tooltip.property("hidden", true);
  d3.select("#graph *").remove();
  d3.select("#graph").property("hidden", true);
}

function openFile(event) {
  //open csv file and convert
  let input = event.target;
  let reader = new FileReader();
  reader.onload = function() {
    loadData(reader.result);
  };
  reader.readAsText(input.files[0]);
}

function loadData(data) {
  stopAnim();
  data = d3.csvParse(data);
  STOCKS = d3.map();
  convertCsv(data);
  TIMES = STOCKS.keys().sort();
  activateControls();
  setSelectTime();
}

function convertCsv(data) {
  /*
  Convert csvarray to map as :
      (key: timestamp
      value: map(
              key : id_station
              value : {
                nbBike,
                nbPlace,
                capacity
              }))
  */
  let t;
  let st;
  for (var i = 0; i < data.length; i++) {
    t = Date.parse(data[i].time);
    st = data[i].stationId;
    if (!STOCKS.has(t)) {
      STOCKS.set(t, d3.map());
    }
    STOCKS.get(t).set(st, {
      nbBike: data[i].nbBikes,
      nbPlace: data[i].nbDocks,
      capacity: data[i].capacity
    });
    if (!LOCATION.has(st)) {
      LOCATION.set(st, {
        "lat": data[i].latitude,
        "lon": data[i].longitude
      });
    }
  }
}

function submit() {
  stopAnim();
  let time = getTimeFromInputs();
  TIMEINDEX = getNearestTime(TIMES, time);
  CURRENTTIME = TIMES[TIMEINDEX];
  printTime(CURRENTTIME);
  document.getElementById("startButton").disabled = false;
  showStations();
}

function showStations() {
  STATIONS = STOCKS.get(CURRENTTIME);
  if (!STATIONS) {
    stopAnim();
    return;
  }
  let stations = STATIONS.keys().sort();

  if (d3.select("#map")
    .select("svg").selectAll("circle").empty()) {
    d3.select("#map")
      .select("svg")
      .append("g")
      .attr("id", "capCircles")
    d3.select("#map")
      .select("svg")
      .append("g")
      .attr("id", "stockCircles")
  }

  let sel = d3.select("#capCircles")
    .selectAll("circle")
    .data(stations);

  sel.attr("r", function(d) { //size of outer circle linked to capacity
      if (STATIONS.get(d).nbBike != "NoData") {
        return STATIONS.get(d).capacity / 5;
      } else {
        return 3;
      }
    })
    .style("stroke", function(d) {
      if (STATIONS.get(d).nbBike != "NoData") {
        return "green";
      } else {
        return "none";
      }
    })


  sel.enter()
    .append("circle")
    .attr("cx", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).x
    })
    .attr("cy", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).y
    })
    .attr("r", function(d) { //size of outer circle linked to capacity
      if (STATIONS.get(d).nbBike != "NoData") {
        return STATIONS.get(d).capacity / 5;
      } else {
        return 3;
      }
    })
    .style("fill", "none")
    .style("stroke", function(d) {
      if (STATIONS.get(d).nbBike != "NoData") {
        return "green";
      } else {
        return "none";
      }
    })
    .attr("pointer-events", "visible")
    .on("mouseover", mouseover)
    .on("mouseleave", mouseleave)
    .on("mousemove", function() {
      Tooltip.style("top", (event.pageY) + "px").style("left", (event.pageX - 240) + "px");
    })

  sel = d3.select("#stockCircles")
    .selectAll("circle")
    .data(stations);

  sel.attr("r", function(d) { // size of inner circle linked to number of bikes
      if (STATIONS.get(d).nbBike != "NoData") {
        return STATIONS.get(d).nbBike / 5;
      } else {
        return 3;
      }
    })
    .style("fill", function(d) { // color of points linked to nbBikes/capacity
      if (STATIONS.get(d).nbBike != "NoData") {
        return "rgb(0," + (STATIONS.get(d).nbBike / STATIONS.get(d).capacity) * 255 + ",0)"
      } else {
        return "red";
      }
    })

  sel.enter()
    .append("circle")
    .attr("cx", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).x
    })
    .attr("cy", function(d) {
      return map.latLngToLayerPoint([LOCATION.get(d).lat, LOCATION.get(d).lon]).y
    })
    .attr("r", function(d) { // size of inner circle linked to number of bikes
      if (STATIONS.get(d).nbBike != "NoData") {
        return STATIONS.get(d).nbBike / 5;
      } else {
        return 3;
      }
    })
    .style("fill", function(d) { // color of points linked to nbBikes/capacity
      if (STATIONS.get(d).nbBike != "NoData") {
        return "rgb(0," + (STATIONS.get(d).nbBike / STATIONS.get(d).capacity) * 255 + ",0)"
      } else {
        return "red";
      }
    })

}


function startAnim() {
  document.getElementById("stopButton").disabled = false;
  document.getElementById("startButton").disabled = true;
  let speed = document.getElementById("speed").value;
  ANIM = true;
  INTERVAL = setInterval(function() {
    anim();
  }, 1000 / speed);
}

function stopAnim() {
  if (ANIM) {
    document.getElementById("stopButton").disabled = true;
    document.getElementById("startButton").disabled = false;
    if (INTERVAL !== null) {
      window.clearInterval(INTERVAL);
      INTERVAL = null;
    }
  }
}

function anim() {
  nextTime();
  showStations();
  if (SELSTATION) {
    updateVerticalLine()
  }
}
