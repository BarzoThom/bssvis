#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import gzip
import shutil
import sys
import json
import csv
import tarfile
from datetime import datetime
import os

TEMP_FOLDER = 'Temp'

stations_locations = 0
station_location_path = 0
st_month = -1
st_year = -1

def progressBar(current, total, barLength = 20):
    percent = float(current) * 100 / total
    dashes   = '#' * int(percent/100 * barLength)
    spaces  = ' ' * (barLength - len(dashes))
    if current != total:
        print('Progress: ['+dashes+spaces+']'+str(int(percent))+'%', end='\r')
    else:
        print('Progress: ['+dashes+spaces+']'+str(int(percent))+'%')


"""
******GBFS******
"""


def gbfs_extract(file_path):
    """
    extract .tar.xz (gbfs) archive to temporary folder

    parameters:
        file_path: path of .tar.xz archive
    return:
    """
    with tarfile.open(file_path) as tf:
        tf.extractall(TEMP_FOLDER)


def gbfs_get_row(time, status, info, error):
    """
    return csv row containing these fields:
    id_station,Date,NbVelos,NbPlace,CapaciteTotale,EnService.
    from info : parsed station_information.json for a station
    and status : parsed station_status.json for a station
    append error line in error array

    parameters:
        time: timestamp
        status: GBFS's station_status of a station
        info: GBFS's station_info of a station
        error: array of error
    return:
        row with
    """
    # convert timestamp to string
    time = time.strftime('%Y-%m-%d %H:%M:%S')
    # check coherence of number of bikes and write error
    if status['num_bikes_available'] + status['num_docks_available'] + status['num_bikes_disabled'] + status['num_docks_disabled'] != info['capacity']:
        error.append(['errorNbBikes', time, status['station_id'], str(status['num_bikes_available']) + '+' + str(status['num_docks_available']
                                                                                                                 ) + '+' + str(status['num_bikes_disabled']) + '+' + str(status['num_docks_disabled']) + '!=' + str(info['capacity'])])
    # check if station is available
    if(status['is_renting']):
        available = 'true'
    else:
        available = 'false'
    return [status['station_id'], info['lat'], info['lon'], time, status['num_bikes_available'], status['num_docks_available'], info['capacity'], available]


def gbfs_get_stations_location(station_information):
    """
    Return a dictionnary with :
        key: station_id
        value: {'lat' : latitude, 'lon' : longitude}

    parameters:
        station_information: parsed station_information.json from GBFS
    return:
        dictionnary
    """
    data = {}
    for s in station_information['data']['stations']:
        data[s['station_id']] = {'lat': s['lat'], 'lon': s['lon']}
    return data


def gbfs_json_to_csv(station_information, station_status, path_out, error_file):
    """
    Convert json data from station_information and station_status to a csv file containing these fields:
    id_station,Date,NbVelos,NbPlace,CapaciteTotale,EnService.
    Errors are written in error_file
    Return number of processed stations

    parameters:
        station_information  : GBFS's json Object
        station_status : GBFS's json Object
        path_out : string representing a path
        error_file : csv.writer
    return:
        nb_stations : integer
    """
    global stations_locations
    global st_month
    global st_year

    # get time of file
    time = datetime.fromtimestamp(
        int(station_information['last_updated']))
    # get location of stations
    if stations_locations == 0 or (time.month != st_month or time.year != st_year):
        if stations_locations != 0:
            save_stations_locations(
                stations_locations, path_out, st_year, st_month)
        st_month = time.month
        st_year = time.year
        stations_locations = gbfs_get_stations_location(station_information)

    output_file = get_csv_writer(path_out, time.date(), 0)
    current_missing_stations = stations_locations.copy()
    nb_stations = 0
    data = []
    error = []
    while nb_stations < len(station_information['data']['stations']):
        info = station_information['data']['stations'][nb_stations]
        status = station_status['data']['stations'][nb_stations]
        if status['station_id'] != info['station_id']:
            # search station in status
            j = 0
            while j < len(station_status['data']['stations']) and info['station_id'] != station_status['data']['stations'][j]['station_id']:
                j += 1
            if j < len(station_status['data']['stations']):
                status = station_status['data']['stations'][j]

        if status['station_id'] == info['station_id']:
            data.append(gbfs_get_row(time, status, info, error))
        else:
            error.append(['different ids from station_information and station_info', time.strftime(
                '%Y-%m-%d %H:%M:%S')])

        # station was processed: remove it from missing station
        try:
            del current_missing_stations[status['station_id']]
        except KeyError:
            stations_locations[info['station_id']] = {
                'lat': info['lat'], 'lon': info['lon']}
        nb_stations += 1

    for missing_station in current_missing_stations:
        # for each missing station write an error line and write a line on output_file with 'noData'
        error.append(['StationNotPresent', time.strftime(
            '%Y-%m-%d %H:%M:%S'), missing_station])
        data.append([missing_station, time.strftime('%Y-%m-%d %H:%M:%S'), 'NoData',
                     'NoData', 'NoData', 'NoData'])

    # write array of data and error to csv files
    output_file.writerows(data)
    error_file.writerows(error)
    return nb_stations


def gbfs_archive_to_csv(path_out, error_file):
    """
    Convert extracted archive in 'archive/' folder to csv files in path_out containing these fields:
    id_station,Date,NbVelos,NbPlace,CapaciteTotale,EnService.
    Errors are written in error_file
    Return number of processed lines

    parameters:
        path_out : string representing a path
        error_file : csv.writer
    return:
        nb_rows : integer
    """

    date = 0
    nb_rows = 0

    for root, dirs, files in os.walk(TEMP_FOLDER + '/archive'):
        for name in sorted(files):
            if name.endswith('.json') and 'info' in name:
                with open(os.path.join(root, name), 'r') as file_info:
                    with open(os.path.join(root, name.replace('info', 'status')), 'r') as file_status:
                        try:
                            # get station_information
                            station_information = json.load(file_info)
                            # get station_status
                            station_status = json.load(file_status)

                            nb_rows += gbfs_json_to_csv(station_information,
                                                        station_status, path_out, error_file)
                        except json.decoder.JSONDecodeError:
                            error_file.writerow(['Corrupted json data', name])

    return nb_rows


def gbfs_to_csv(path_out):
    """
    Convert extracted GBFS data to csv files in path_out containing these fields:
    id_station,Date,NbVelos,NbPlace,CapaciteTotale,EnService.
    Errors are written in a csv file
    return number of row written in csv files

    parameters:
        path_out : string representing a path
    return:
        nb_rows : integer
    """
    with open(os.path.join(path_out, 'error.csv'), 'w', newline='') as error_file:
        # init csv writer
        error_file = csv.writer(error_file)
        error_file.writerow(['id_station', 'Date', 'Type', 'Info'])
        nb_rows = 0
        i = 0
        for root, dirs, files in os.walk(TEMP_FOLDER):
            for name in sorted(files):
                # for each archive :
                try:
                    # extract archive
                    archive = tarfile.open(os.path.join(root, name), 'r')
                    archive.extractall(TEMP_FOLDER + '/archive')
                    archive.close()
                    # convert data to csv
                    nb_rows += gbfs_archive_to_csv(path_out, error_file)
                    # remove extracted archive
                    shutil.rmtree(TEMP_FOLDER + '/archive')

                except tarfile.ReadError:
                    print('Corrupted archive : ' + name)
                    error_file.writerow(['Corrupted archive', name])
                    pass
                except EOFError:
                    print('Corrupted archive : ' + name)
                    error_file.writerow(['Corrupted archive', name])
                    pass
                i += 1
                progressBar(i, len(files))
        save_stations_locations(
            stations_locations, path_out, st_year, st_month)
        return nb_rows


"""
****** IFSTTAR ******
Data from : http://vlsstats.ifsttar.fr/rawdata/RawData/
"""


def ifsttar_extract(file_path):
    """
    extract .gz (IFSTTAR) archive to temporary folder

    parameters:
        file_path: path of .gz archive
    return:
    """
    if station_location_path == 0:
        print('To convert IFSTTAR a folder with station_location.json files must be provided')
    os.mkdir(TEMP_FOLDER)
    new_file_path = TEMP_FOLDER + '/' + \
        file_path.split('/')[-1].replace('.gz', '.json')
    with gzip.open(file_path, 'rb') as file_in:
        #     with open(TEMP_FOLDER + '/temp.gz', 'wb') as file_out:
        #         file_out.write(file_in.read())
        # with gzip.open(TEMP_FOLDER + '/temp.gz', 'rb') as file_in:
        with open(new_file_path, 'wb') as file_out:
            file_out.write(file_in.read())
    # os.remove(TEMP_FOLDER + '/temp.gz')


def read_position(year, month):
    """
    Read position from position file
    """
    # no station_location.json before 2015/02 :
    if year < 2015:
        year = 2015
    if year == 2015 and month == 2:
        month = 3

    if(month < 10):
        month = '0' + str(month)
    POS_file = station_location_path + \
        str(year) + "/station_location_" + \
        str(year) + "-" + str(month) + ".json"
    with open(POS_file, 'r') as infile:
        pos = json.load(infile)
    return pos


def ifsttar_to_csv(path_out):
    """
    Convert extracted IFSTTAR data to a csv file output_file containing these fields:
    id_station,Date,NbVelos,NbPlace,CapaciteTotale,EnService.
    return number of row written in csv files

    parameters:
        path_out : string representing a path
    return:
        nb_rows : integer
    """
    # TODO: write 'enter' before '['

    nb_rows = 0
    nolocation = 0
    for root, _, files in os.walk(TEMP_FOLDER):
        for name in sorted(files):
            with open(os.path.join(root, name), 'r') as file_in:
                pos = 0
                date = 0
                i = 0
                line = file_in.readline()
                while line:
                    try:
                        data = json.loads(line)
                        csv_data = []
                        time = datetime.fromtimestamp(
                            int(data[0]['download_date']))
                        if pos == 0:
                            pos = read_position(time.year, time.month)

                        if date != time.date():
                            date = time.date()
                            csv_out = get_csv_writer(path_out, date, 1)

                        for d in data:
                            available = 'false'
                            try:
                                if(d['status'] == 'OPEN'):
                                    available = 'true'
                            except KeyError as e:
                                # print('error : missing ' + str(e) +
                                #       ' field at line ' + str(i + 1))
                                pass
                            try:
                                lat = pos[str(d['number'])]['lat']
                                lon = pos[str(d['number'])]['lon']
                            except KeyError as e:
                                lat = 0
                                lon = 0
                                nolocation += 1
                            try:
                                csv_data.append(
                                    [d['number'], lat, lon, time.strftime('%Y-%m-%d %H:%M:%S'), d['available_bikes'], d['available_bike_stands'], d['bike_stands'], available])
                            except KeyError as e:
                                # print('error : missing ' + str(e) +
                                #       ' field at line ' + str(i + 1))
                                pass
                            nb_rows += 1
                        csv_out.writerows(csv_data)
                    except json.decoder.JSONDecodeError:
                        # print('error : line ' + str(i + 1) + ' corrupted')
                        pass
                    line = file_in.readline()
                    i += 1

    if nolocation > 0:
        print("error : missing location for some stations")
    return nb_rows


"""
****** The Open Bus ******
Data from : https://www.theopenbus.com/raw-data.html
"""


def open_bus_extract(file_path):
    """
        extract .zip (The Open Bus) archive to temporary folder

        parameters:
            file_path: path of .zip archive
        return:
    """
    shutil.unpack_archive(file_path, TEMP_FOLDER)


def open_bus_get_time(row):
    """
    return datetime object from row of csv file from The Open Bus
    parameters:
        row : parsed csv row
    return:
        datetime object
    """
    t = row[2].split('-')
    year = 2000 + int(t[0])
    month = int(t[1])
    day = int(t[2])
    hour = int(row[3])
    if row[5] == '1':
        hour = (hour + 12) % 24
    minute = int(row[4])
    return datetime(year, month, day, hour, minute)


def open_bus_get_stations_locations(file_name):
    """
    Return a dictionnary with :
        key: station_id
        value: {'lat' : latitude, 'lon' : longitude}

    parameters:
        file_name : path of csv file
    return:
        dictionnary
    """

    with open(file_name, 'r') as file_in:
        csv_in = csv.reader(file_in, delimiter='\t')
        data = {}
        i = 0
        for row in csv_in:
            if i > 0:
                if row[0] not in data:
                    try:
                        data[row[0]] = {'lat': float(
                            row[9]), 'lon': float(row[10])}
                    except ValueError as e:
                        pass
            i += 1
        return data


def open_bus_to_csv(path_out):
    """
    Convert extracted The Open Bus data to csv files in path_out containing these fields:
    id_station,Date,NbVelos,NbPlace,CapaciteTotale,EnService.
    return number of row written in csv files

    parameters:
        path_out : string representing a path
    return:
        nb_rows : integer
    """
    global stations_locations
    global st_month
    global st_year

    nb_rows = 0
    for root, _, files in os.walk(TEMP_FOLDER):
        for name in sorted(files):
            if 'bikeshare' in name:
                print(os.path.join(root, name))
                with open(os.path.join(root, name), 'r') as file_in:
                    csv_in = csv.reader(file_in, delimiter='\t')
                    i = 0
                    date = 0
                    for row in csv_in:
                        if i > 0:
                            try:
                                time = open_bus_get_time(row)
                                if time.date() != date:
                                    date = time.date()
                                    csv_out = get_csv_writer(path_out, date, 2)
                                # get location of stations
                                if stations_locations == 0 or (time.month != st_month or st_year != time.year):
                                    stations_locations = open_bus_get_stations_locations(
                                        os.path.join(root, name))
                                    save_stations_locations(
                                        stations_locations, path_out, st_year, st_month)
                                    st_month = time.month
                                    st_year = time.year
                                if(row[11] == '1'):
                                    available = 'true'
                                else:
                                    available = 'false'
                                csv_out.writerow([row[0], row[9], row[10], time.strftime(
                                    '%Y-%m-%d %H:%M:%S'), row[6], row[7], row[8], available])
                                nb_rows += 1
                            except IndexError as e:
                                # print('line ' + str(i) + ' corrupted')
                                pass
                        i += 1

    return nb_rows


def save_stations_locations(stations_locations, path_out, st_year, st_month):
    """
    save dictionnary of locations in .json file inside path_out folder
    parameters:
        stations_locations : dictionnary of stations location
        path_out : string representing a path
        st_year : year
        st_month : month
    return:
        nb_rows : integer
    """
    if(st_month < 10):
        st_month = '0' + str(st_month)
    else:
        st_month = str(st_month)
    with open(os.path.join(path_out, 'stations_location_' + str(st_year) + '-' + st_month + '.json'), 'w') as f_out:
        json.dump(stations_locations, f_out)


def get_csv_writer(path_out, date, format):
    """
    Return a csv writer corresponding to date
    parameters:
        path_out : string representing a path
        date : datetime
        format : integer
    return:
        csv.writer
    """
    # if format == 0:
    #     prefix = 'stock_gbfs_'
    # elif format == 1:
    #     prefix = 'stock_ifsttar_'
    # elif format == 2:
    #     prefix = 'stock_tob_'

    prefix = 'stock_'
    folder = os.path.join(path_out, date.strftime("%Y"))
    if not os.path.isdir(folder):
        os.mkdir(folder)
    folder = os.path.join(folder, date.strftime("%Y-%m"))
    if not os.path.isdir(folder):
        os.mkdir(folder)
    file_name = os.path.join(folder, prefix +
                             date.strftime("%Y-%m-%d") + '.csv')
    if not os.path.isfile(file_name):
        file_out = open(file_name, 'w')
        csv_out = csv.writer(file_out)
        csv_out.writerow(
            ['stationId', 'latitude', 'longitude', 'time', 'nbBikes', 'nbDocks', 'capacity', 'inService'])
    else:
        file_out = open(file_name, 'a')
        csv_out = csv.writer(file_out)
    return csv_out


def get_data_format(file_path):
    """
    get data format of archive :
        0 : gbfs
        1 : ifsttar
        2 : openbus
    parameters:
        file_path : path of the archive
    return:
        integer
    """
    if file_path.endswith('tar.xz'):
        return 0
    if file_path.endswith('.gz'):
        return 1
    if file_path.endswith('.zip'):
        return 2
    raise FileNotFoundError('Incompatible File : ' + file_path)


def to_csv(path_in, path_out):
    """
    Convert data from GBFS, IFSTTAR or The Open Bus to csv files in path_out containing these fields:
    id_station,Date,NbVelos,NbPlace,CapaciteTotale,EnService.
    return number of row written in csv files

    parameters:
        path_in : paths of the archive containing the data
        path_out : path of the folder where csv files will be written
    return:
        nb_rows : integer
    """
    if os.path.isdir(TEMP_FOLDER):
        shutil.rmtree(TEMP_FOLDER)

    nb_rows = 0
    try:
        format = get_data_format(path_in)
    except FileNotFoundError as e:
        print('error : Incompatible archive')
        return 0
    print("extracting ...")
    if format == 0:
        gbfs_extract(path_in)
        print("converting GBFS ...")
        nb_rows = gbfs_to_csv(path_out)
    elif format == 1:
        ifsttar_extract(path_in)
        print("converting IFSTTAR ...")
        nb_rows = ifsttar_to_csv(path_out)
    elif format == 2:
        open_bus_extract(path_in)
        print("converting Openbus ...")
        nb_rows = open_bus_to_csv(path_out)
    shutil.rmtree(TEMP_FOLDER)
    return nb_rows


"""
-------MAIN-------
"""
#todo creer un repertoite pour l'annee

print('\nConversion to .csv')
if len(sys.argv) >= 3:
    if len(sys.argv) == 4:
        station_location_path = sys.argv[3]
    path_in = sys.argv[1]
    path_out = sys.argv[2]
    # if os.path.isdir(path_out):
    #     shutil.rmtree(path_out)
    if not os.path.isdir(path_out):
        os.mkdir(path_out)
    if  os.path.isdir(path_in):  # it's a directory
        for root, _, files in os.walk(path_in):
            for name in sorted(files):
                print(name)
                nb_rows = to_csv(os.path.join(root, name), path_out)
                print(nb_rows)
    else:
        print(path_in)
        nb_rows = to_csv(path_in, path_out)
        print(nb_rows)
    print('Done')
else:
    print('usage : conversion.py <inputsarchive> <outputfolder> [stationlocation]')
    print('usage : conversion.py <inputfolder> <outputfolder>  [stationlocation]')
    # todo path location a modifier ?
