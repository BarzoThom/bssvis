#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import shutil
import sys
import csv
from datetime import datetime
import os


def compare_folder(path1, path2, path_out):
    """
    Compare 2 folder containing .csv files and write result ind path_out folder

    parameters:
        path1 : path of first directory
        path2 : path of second directory
        path_out : string representing a path
    return:
    """
    if os.path.isdir(path_out):
        shutil.rmtree(path_out)
    os.mkdir(path_out)
    for root, dirs, files in os.walk(path1):
        for name in sorted(dirs):
            if(os.path.isdir(os.path.join(path2, name))):
                # we can compare a part of the month
                compare_month(os.path.join(path1, name), os.path.join(
                    path2, name), path_out)


def compare_month(path1, path2, path_out):
    """
    Compare 2 folder containing .csv files for one month and write result ind path_out folder

    parameters:
        path1 : path of first directory
        path2 : path of second directory
        path_out : string representing a path
    return:
    """
    for root, dirs, files in os.walk(path1):
        for name in sorted(files):
            if(os.path.isfile(os.path.join(path2, name))):
                compare_day(os.path.join(path1, name),
                            os.path.join(path2, name), path_out)


def compare_day(path1, path2, path_out):
    """
    Compare 2 .csv files and write result ind path_out folder

    parameters:
        path1 : path of first file
        path2 : path of second file
        path_out : string representing a path
    return:
    """
    data1 = {}
    data2 = {}
    with open(path1, 'r') as file_1:
        with open(path2, 'r') as file_2:
            csv_1 = csv.reader(file_1)
            csv_2 = csv.reader(file_2)
            i = 0

            for row in csv_1:
                if i > 0:
                    time = get_rouded_timestamp(row[3])
                    if time not in data1:
                        data1[time] = {}
                    data1[time][row[0]] = row[4]
                i += 1

            i = 0
            for row in csv_2:
                if i > 0:
                    time = get_rouded_timestamp(row[3])
                    if time not in data2:
                        data2[time] = {}
                    data2[time][row[0]] = row[4]
                i += 1

            date = datetime.fromtimestamp(time)
            csv_out = get_csv_writer(path_out, date)
            for time in data1:
                if time in data2:
                    rows = compare_time(time, data1, data2)
                    csv_out.writerows(rows)


def compare_time(time, data1, data2):
    """
    Compare data at a specific time

    parameters:
        time :
        data1 : array of data from parsed .csv
        data2 : array of data from parsed .csv
    return:
        array
    """
    res = []
    data1 = data1[time]
    data2 = data2[time]
    for s in data1:
        if s in data2:
            if(data1[s] != "NoData" and data2[s] != "NoData"):
                res.append([s, datetime.fromtimestamp(time).strftime(
                    '%Y-%m-%d %H:%M:%S'), data1[s], data2[s], int(data1[s]) - int(data2[s])])
            else:
                res.append([s, datetime.fromtimestamp(time).strftime(
                    '%Y-%m-%d %H:%M:%S'), data1[s], data2[s], "NoData"])
    return res


def get_rouded_timestamp(csv_date):
    """
    convert string date from scv file to timestamp
    and round it to 5 minute
    parameters:
        csv_date: datetime obect
    return:
        timestamp
    """
    date = datetime.strptime(csv_date, '%Y-%m-%d %H:%M:%S')
    return int(date.timestamp() / 300) * 300


def get_csv_writer(path_out, date):
    """
    Return a csv writer for corresponding date: one folder for each month containing one file for each day
    parameters:
        path_out : string representing a path
        date : datetime
        format : integer
    return:
        csv.writer
    """

    prefix = 'comp_'

    folder = os.path.join(path_out, date.strftime("%Y"))
    if not os.path.isdir(folder):
        os.mkdir(folder)
    folder = os.path.join(folder, date.strftime("%Y-%m"))
    if not os.path.isdir(folder):
        os.mkdir(folder)
    file_name = os.path.join(folder, prefix +
                             date.strftime("%Y-%m-%d") + '.csv')
    if not os.path.isfile(file_name):
        file_out = open(file_name, 'w')
        csv_out = csv.writer(file_out)
        csv_out.writerow(
            ['stationId', 'time', 'nbBikes1', 'nbBikes2', 'diff'])
    else:
        file_out = open(file_name, 'a')
        csv_out = csv.writer(file_out)
    return csv_out

"""
-------MAIN-------
"""


if len(sys.argv) == 4:
    path_1 = sys.argv[1]
    path_2 = sys.argv[2]
    path_out = sys.argv[3]
    compare_folder(path_1, path_2, path_out)
else:
    print('usage : comparison.py <folder1> <folder2> <outputfolder>')
