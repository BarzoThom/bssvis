#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json
import os
import pandas as pd


def generate_location_json(path_in, path_out):
    """
    Generate station_location.json for each month with citibike tripdata in path_in folder
    """
    global stations_locations
    global st_month
    global st_year

    location = {}
    nb = 0
    # todo : sort by time
    for root, _, files in os.walk(path_in):
        for name in sorted(files):
            if '.csv' in name and not '._' in name:
                print(name)
                st_year = name[0:4]
                st_month = name[4:6]
                if not os.path.isdir(os.path.join(path_out,st_year)):
                    os.mkdir(os.path.join(path_out,st_year))

                data = pd.read_csv(os.path.join(root, name))
                try:
                    v1 = data[['start station id','start station latitude','start station longitude']]
                    v2 = data[['end station id','end station latitude','end station longitude']]
                    v1.rename(inplace=True,columns={'start station id':'id','start station latitude':'lat','start station longitude':'lon'})
                    v2.rename(inplace=True,columns={'end station id':'id','end station latitude':'lat','end station longitude':'lon'})
                except KeyError as e:
                    v1 = data[['Start Station ID','Start Station Latitude','Start Station Longitude']]
                    v2 = data[['End Station ID','End Station Latitude','End Station Longitude']]
                    v1.rename(inplace=True,columns={'Start Station ID':'id','Start Station Latitude':'lat','Start Station Longitude':'lon'})
                    v2.rename(inplace=True,columns={'End Station ID':'id','End Station Latitude':'lat','End Station Longitude':'lon'})
                res = v1.append(v2)
                res.drop_duplicates('id', keep='first', inplace=True)
                res.set_index('id',inplace=True)
                res.to_json(os.path.join(path_out,st_year,'station_location_' + str(st_year) + '-' + st_month + '.json'),orient='index')



"""
-------MAIN-------
"""


print('\ngeneration station_location.json...')
if len(sys.argv) == 3:
    path_in = sys.argv[1]
    path_out = sys.argv[2]
    generate_location_json(path_in, path_out)
else:
    print('usage : location.py <inputfolder> <outputfolder>')
