### Convert archive to .csv files:

`conversion.py <inputsarchive> <outputfolder> [stationlocation]`  
`conversion.py <inputfolder> <outputfolder>  [stationlocation]`

'inputsarchive' is a path of compatible archive containing .json files or .csv files depending on the format.  
'inputfolder' is a path of a folder containing compatibles archives.
'outputfolder' will contain the result of the conversion in .csv files and the location of stations in .json files.  
'stationlocation' is a path of a folder containing station_location.json used in converting data from IFSTTAR  

##### compatible archives:

-   GBFS
-   IFSTTAR : <http://vlsstats.ifsttar.fr/rawdata/RawData/>
-   The Open Bus : <https://www.theopenbus.com/raw-data.html>

**'output folder' will be strutured like this:**  

    output folder  
      |──YYYY
      |    |──YYYY-MM  
      |    |    |──stock_YYYY-MM-DD.csv  
      |    |    └──...  
      |   ...
     ...
**.csv files will contain these fields:**  
_stationId, latitude, longitude, time, nbBikes, nbDocks, capacity, inService_

### Compare .csv files:

`comparison.py <folder1> <folder2> <output folder>`

'folder1' and 'folder2' are 2 directory containing .csv files created by conversion.py.  
'output folder' will contain the result of the comparison in .csv files.

**'folder1' and 'folder2' must be strutured like this:**  

    folder  
      |──YYYY
      |    |──YYYY-MM   
      |    |    |──stock_YYYY-MM-DD.csv  
      |    |    └──...  
      |   ...
     ...

**'output folder' will be strutured like this:**

    output folder  
      |──YYYY
      |    |──YYYY-MM   
      |    |    |──comp_YYYY-MM-DD.csv  
      |    |    └──...  
      |   ...
     ...

**.csv files will contain these fields:**  
_stationId, time, nbBikes1, nbBikes2, diff_

### Generate station_location.json:

`location.py <inputfolder> <outputfolder>`

'inputfolder' contain citybike tripdata files  
'output folder' will contain station_location.json for each month of data.
